---
theme: default
background: ./assets/img/portada.png
# class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
transition: slide-left
title: Hack N Beers Panamá Oeste Vol.2
hideInToc: true
---

<br>
<br>
<br>
<br>
<br>
<br>
<br>

## Aqui iria un titulo, si lo tuviera

#### Hack & Beers - Westside vol.2


<div class="abs-bl mx-14 my-12 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-16">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-16">
</div>

---
hideInToc: true
layout: image-right
image: ./assets/img/0_2.png
---

# Agenda

<br>
<Toc maxDepth="2" class="text-sm"></Toc>



<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Intro
class: text-center
transition: slide-up
layout: cover
background: ./assets/img/fondo2.png
---

<div class="bg-red-500 bg-opacity-30">

# De que hablaremos hoy?

</div>

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Que es el Threat Hunting?
level: 2
transition: slide-up
---

# Que es el Threat Hunting?

<br>
<br>

<div class="grid grid-cols-3 gap-4">
<div>
<img src="assets/img/infected.png" class="h-14 b-4" style="background: #e5e7eb;">
<br>
<h2 class="text-red-500">Detectar</h2> 

Reconocer los comportamientos inusuales que podrian ser indicadores de compromiso en un incidente de ciberseguridad, dentro de una organizacion.
</div>
<div>
<img src="assets/img/hacker.png" class="h-14 b-4" style="background: #e5e7eb;">
<br>
<h2 class="text-red-500">Investigar</h2> 

Definir lineas de investigacion, dentro de las cuales se busca encontrar y definir formalmente los indicadores de compromiso que corresponden a una potencial 

</div>

<div>
<img src="assets/img/cyber-security.png" class="h-14 b-4" style="background: #e5e7eb;">
<br>
<h2 class="text-red-500">Responder</h2> 

Construir una estrategia de respuesta para un incidente, con la cual se procedera a contestar a dicho incidente.

</div>
</div>




<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Threat Hunting en el mundo Real
class: text-center
level: 2
---


# Threat Hunting en el mundo Real

<div class="grid grid-cols-3 gap-4 items-center">

<div>
<img class="center b-8" style="background: #e5e7eb;" src="assets/img/lockbit.png">
</div>
<div>
<img src="assets/img/Hive_Ransomware.jpg">
</div>
<div>
<img src="assets/img/royal.png">
</div>
<div>
<img class="center" src="assets/img/1660581017473-guacamaya-hackers.webp">
</div>
<div>
<img src="assets/img/GettyImages_1269472892.61fc1ac2227b2.avif">
</div>
<div>
<img src="assets/img/anonymous.jpg">
</div>
</div>


<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>

---
title: Caso de Estudio
class: text-center
layout: cover
background: ./assets/img/fondo2.png
transition: slide-up
---

<div class="bg-red-500 bg-opacity-30">

# Caso de Estudio

</div>

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Servidor Infectado
class: text-center
level: 2
layout: image
image: ./assets/img/mstsc_hrptfhguqX.png
transition: slide-up
---


<div absolute bottom-10 left-0 right-0 mx-50 my-5>
<div bg-red-500 bg-opacity-50>

# Servidor Infectado

</div>
</div>
<!-- 
<div class="grid grid-cols-2 gap-4 items-center">
<div>
<img src="assets/img/mstsc_hrptfhguqX.png">
</div>
<div>
<img src="assets/img/mstsc_iHIhtPNGGS.png">
</div>
</div>
-->

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Malware 
class: text-center
level: 2
layout: image
image: ./assets/img/mstsc_iHIhtPNGGS.png
transition: slide-up
---




<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Rastreando y encontrando el servidor c2 del Atacante
class: text-center
level: 2
layout: image
image: ./assets/img/firefox_U9KesJXz1F.png
transition: slide-up
---



<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Analisis del C2
level: 2
transition: slide-up
---

# Analisis del C2

<div class="grid grid-cols-2 gap-4 items-center place-items-center">
<div>
<img class="h-80" src="assets/img/firefox_Z8qhckzbFY.png">
</div>
<div>
<img class="h-70" src="assets/img/mstsc_rlcwk3Nfmd.png">
</div>
</div>

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Vulnerando el servidor del atacante
level: 2
transition: slide-up
---

# Analisis del C2

<div class="grid grid-row-2 gap-4 items-center place-items-center">
<div>
<img src="assets/img/mstsc_5Xe4WOgMQ5.png">
</div>
<div>
<img src="assets/img/mstsc_sKVcwIZwSK.png">
</div>
</div>

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: Vulnerando el servidor del atacante
hideInToc: true
level: 2
---

# Analisis del C2

<div class="grid grid-cols-2 gap-4 place-items-center">
<div>

Errores del atacante, que permitirian una investigacion mas profunda:

- Contraseña en scripts
- Direcciones Ips
- Nombres o Aliases
- Wallets de XMR
- Scripts, Implants, etc.

</div>
<div>
<img class="h-100" src="assets/img/mstsc_UHKNSJnuFP.png">
</div>

</div>

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>
---
title: ¿Otras Lineas de Investigación?
hideInToc: true
---

# ¿Otras Lineas de Investigación?

<img src="assets/img/firefox_SvDtc8DT3w.png">

<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>

---
title: Conclusiones, Moralejas y Preguntas.
layout: center
---

<div class="flex">
<img src="assets/img/643765ba04bd1.webp" >


<div class= "p-4">

# Conclusiones, Moralejas y Preguntas.

</div>


</div>



<div class="abs-bl mx-14 my-8 flex">
  <img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-8">
  <div></div>
  <img src="assets/img/white-v.png" class="ml-4 h-8">
</div>

---
title: Fin!
layout: center
hideInToc: true
---

<div class="grid grid-cols-2 gap-4 content-center">
<div>
<img src="assets/img/cropped-logo-prubea-comunidad-dojo.png" class="h-18">
<br>
https://comunidaddojo.org/
</div>
<div>
<img src="assets/img/white-v.png" class="ml-4 h-18 self-center">
<br>
https://blog.toadsec.io/
</div>

# ¡Gracias!
</div>
<br>

